class Piece

    setIndex: (@index) ->

    setLength: (@length) ->

    setSwitch: (@has_switch) ->

    setRadius: (@radius) ->

    setAngle: (@angle) ->

    getIndex: =>
        @index

    getLength: =>
        @length

    getSwitch: =>
        @has_switch

    getRadius: =>
        @radius

    getAngle: =>
        @angle

    isCurve: =>
        this.getRadius() > 0 || this.getAngle() > 0

class Track 
    
    constructor: (@id) ->
        @pieces = new Array()
        @number_of_pieces = 0

    getId: =>
        @id

    getName: =>
        @name

    setName: (@name) ->

    info: =>
        "Track: #{@name} [#{@id}] consists of #{@number_of_pieces} pieces"

    addPiece: (piece) ->

        @number_of_pieces = @pieces.push(piece)

        index = @number_of_pieces - 1

        piece.setIndex(index)

        return index

    getPiece: (index) ->

        return @pieces[index]

    getPieceAfter: (index) ->

        index++

        if (index >= @pieces.length)
            index = 0

        return @getPiece(index)

    getPiecesToNextCurve: (index) ->

        piece = @getPiece(index)

        pieces = 0

        pieces++ until (piece = @getPieceAfter(piece.getIndex())).isCurve() or pieces > @number_of_pieces

        if pieces > @number_of_pieces then undefined else pieces

exports.Track = Track
exports.Piece = Piece