
class Car

    constructor: (@id) ->
        @throttle = 0

        decimals = 5
        @decimalFactor = Math.pow(10, decimals)

    getId: =>
        return @id

    getMaxThrottle: =>
        this.convertNumber(@max, true)

    setMaxThrottle: (max) ->
        @max = this.convertNumber(max)

    getThrottle: =>
        return this.convertNumber(@throttle, true)

    setThrottle: (throttle) ->
        if throttle > this.getMaxThrottle()
            throttle = this.getMaxThrottle()

        else if throttle < 0.00
            throttle = 0.00

        @throttle = this.convertNumber(throttle)

    getSpeed: => 
        'my speed is not implemented!'

    convertNumber: (number, toFloat = false) ->
        if toFloat is true
            return number / @decimalFactor
        else
            return number * @decimalFactor


class Lada extends Car
    
    ## REMOVE/REFACTOR METHOD ##
    race: (positions) -> 
        #myPos = currentTrack.getMyPosition(positions, @id)  
        #currentPiece = currentTrack.getCurrentPiece(myPos)

        if @throttle < @max 
            this.setThrottle(@throttle + 0.3)   

        return this.getThrottle()

exports.Car = Car
exports.Lada = Lada
