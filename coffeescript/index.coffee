# Racecar bot interface

# Include racecar bot logic
#=require <buildcomment.coffee>
#=require <logic.coffee>
#=require <car.coffee>
#=require <debug.coffee>
#=require <ai.coffee>
#=require <track.coffee>

# Require node modules
net        = require('net')
JSONStream = require('JSONStream')

# Command line args
serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]
mode = process.argv[6]


if mode?
    mode = mode.toLowerCase()

# EVIL globals
myLada = null
currentTrack = null
gameTick = null
storedResponses = []
maxStoredResponses = 50
ai = null
car_positions = null

# Create log file name
logFile = getUniqueFileName('log.txt')

client = net.connect serverPort, serverHost, () ->
    #send({ msgType: 'join', data: { name: botName, key: botKey }})
    send({
        "msgType": 'joinRace',
        "data": {
            botId: {
                "name": botName + "",
                "key": botKey
            }
            "trackName": "keimola",
            #"trackName": "germany",
            #"trackName": "usa",
            "carCount": 2
        }
    })

send = (json) ->
    # Include game tick
    if isNaN(gameTick) isnt true
        json.gameTick = gameTick
    
    debugGameTick(json, gameTick)
    client.write JSON.stringify(json)
    client.write '\n'

ping = ->
    send({msgType: 'ping'})

jsonStream = client.pipe(JSONStream.parse())

jsonStream.on 'data', (response) ->
    try
        logData(response, logFile)

        # Store game tick
        gameTick = if response.hasOwnProperty('gameTick') then response.gameTick else '[no tick]'

        # Store x amount of responses for debugging: storing last response is not enough!
        storeResponses(response, storedResponses, maxStoredResponses)

        if response.msgType is 'yourCar'
            myLada = new Lada(response.data.color)
            myLada.setThrottle(0.0)
            myLada.setMaxThrottle(1.0);

            console.log "** Lada's id: #{myLada.getId()} **" 

        else if response.msgType is 'carPositions'
            my_car_information = car_positions.getMyPosition(response.data, myLada.getId())

            current_piece_index = my_car_information.piecePosition.pieceIndex

            current_piece = currentTrack.getPiece(current_piece_index)
            next_piece = currentTrack.getPieceAfter(current_piece_index)
            pieces_to_next_curve = currentTrack.getPiecesToNextCurve(current_piece_index)

            console.log current_piece_index + ': ' + current_piece.isCurve() + ' -> ' + next_piece.isCurve() + ' d: ' + pieces_to_next_curve
            send(ai.whatNext(pieces_to_next_curve, my_car_information.angle))

        else if response.msgType is 'spawn'
            if response.data.color is myLada.getId()
                console.log '** Lada restored on the track! **'

        else if response.msgType is 'gameInit'
            currentTrack = new Track(response.data.race.track.id)
            currentTrack.setName(response.data.race.track.name)

            for piece_data in response.data.race.track.pieces
                do (piece_data) ->
                    piece = new Piece
                    piece.setLength(piece_data.length)
                    piece.setSwitch(piece_data.switch)
                    piece.setRadius(piece_data.radius)
                    piece.setAngle(piece_data.angle)

                    currentTrack.addPiece(piece)

            car_positions = new CarPositions(@track)

            console.log currentTrack.info()
            ai = new GrandmaAI(myLada, currentTrack)
            console.log ai.hello()

        else if response.msgType is 'join'
            console.log 'Joined'
        else if response.msgType is 'gameStart'
            console.log 'Race started!'
        else if response.msgType is 'gameEnd'
            console.log 'Race ended, results:'

            bestTimes = []
            i = 0

            for time in response.data.bestLaps
                if time.result.hasOwnProperty('millis')
                    bestTimes.push((time.result.millis/1000) % 60)
                else
                    bestTimes.push('[no time]')

            for result in response.data.results
                console.log "#{i}. #{result.car.name} [id: #{result.car.color}], best time: #{bestTimes[i]}"        
                i++

        else if response.msgType is 'tournamentEnd'
            console.log 'Tournament ended'

        else if response.msgType is 'crash'
            if response.data.color is myLada.getId()
                console.log '** Oh no, Lada has crashed! **'
                debugCrash(storedResponses, myLada.getId())

                # LIMIT THROTTLE
                myLada.setMaxThrottle(myLada.getMaxThrottle() - 0.1)
                myLada.setThrottle(0.0)
            else
                console.log "A #{response.data.id} [#{response.data.name}] car crashed!"

        else if response.msgType is 'dnf'
            if response.data.color is myLada.getId()
                console.log '** Oh no, Lada was disqualified: #{response.data.reason}! **'
            else
                console.log "A #{response.data.id} [#{response.data.name}] car was disqualified!"            
        
        else if response.msgType is 'lapFinished'
            if response.data.color is myLada.getId()
                console.log 'Finished lap in #{response.data.lapTime.millis} milliseconds, ranking: #{response.data.ranking.overall}'

        else if response.msgType is 'finish'
            if response.data.color is myLada.getId()
                console.log '** Lada finished the race! **'
            else
                console.log "A #{response.data.id} [#{response.data.name}] car finished the race!"  

        else if response.msgType is 'turboAvailable'
            console.log "Turbo is available: #{response.data.turboFactor}"    

        else if response.msgType is 'error'
            console.log "\n#### API ERROR: #{response.data} on tick #{gameTick} ####\n"  

        else
            console.log '** Unexpected server message: **'
            console.log JSON.stringify(response, null, '\t')
            ping()

    catch e
        console.log "\n#### EXCEPTION: #{e.message} on message #{response.msgType} with tick being #{gameTick} ####"
        console.log "Stacktrace:\n\n#{e.stack}\n"
    
jsonStream.on 'error', ->
    console.log '\n#### JSON STREAM ERROR ####\n'
