IDEAS/NOTES/ETC
===============

Optimising
-----------

- friction: can be calculated from slipdist, angle etc.
- bends: switch to inner lane before a bend if the bend is tight enough (angle)
- there must be a upper bound for slipdist before crashing?

Learning
---------

- on the qualifying rounds, the bot can estimate the max speed for bends: 
  start the race with 0.5 throttle, increase till max slipdist

Misc
----

- the bot MUST reply on each tick!