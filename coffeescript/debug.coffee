# Debug utilities

fs = require('fs')

getUniqueFileName = (fname) ->
    count = 1
    parts = fname.split('.')

    while fs.existsSync('./logs/' + fname)
        fname = parts[0] + count + '.' + parts[1]
        count++

    return fname

logData = (data, fname) ->
    if mode is "debug"
        fs.appendFile('./logs/' + fname, JSON.stringify(data) + '\n\n')

debugGameTick = (lastResponse, gameTick) ->
    if mode is "debug"
        if isNaN(gameTick) isnt true
            if lastResponse.gameTick isnt gameTick
                tickDiff = Math.abs(lastResponse.gameTick - gameTick)
                console.log '\n#### BOT NOT SYNCING WITH SERVER, TICK DIFFERENCE: #{tickDiff} ####\n'

debugCrash = (storedResponses, myId) ->
    if mode is "debug"
        lastInfo = {angle: 0}

        for response in storedResponses
            # Sometimes the server sends NULL data...
            if response.data isnt null and response.msgType is 'carPositions'

                carPos = car_positions.getMyPosition(response.data, myId)

                if carPos.angle > lastInfo.angle
                    lastInfo = carPos
                    lastInfo.gameTick = response.gameTick

        if lastInfo.angle != 0
            console.log "Reason: crashed in a bend with angle of #{lastInfo.angle} using throttle of #{myLada.getThrottle()} on tick #{lastInfo.gameTick}"
        else
            console.log 'Reason: crashed on a straight line?'

        console.log "Last position: pieceIndex #{lastInfo.piecePosition.pieceIndex}, inPieceDistance #{lastInfo.piecePosition.inPieceDistance}"

storeResponses = (currentResponse, storedResponses, limit) ->
    if storedResponses.length >= limit
        storedResponses.shift()

    storedResponses.push(currentResponse)
