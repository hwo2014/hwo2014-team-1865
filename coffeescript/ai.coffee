class GrandmaAI
    constructor: ( @car, @track ) ->
        @last_angle = 0.00
        @last_throttle = 1.00

    whatNext: ( pieces_to_next_curve, current_car_angle ) ->

        absolute_angle = Math.abs(current_car_angle)
        angle_delta = absolute_angle - @last_angle

        throttle_to_use = this.decideThrottle(absolute_angle, angle_delta)

        @car.setMaxThrottle(@decideMaxThrottle(pieces_to_next_curve))

        @car.setThrottle(throttle_to_use);

        throttle_to_use = @car.getThrottle()

        @last_throttle = throttle_to_use
        @last_angle = absolute_angle

        console.log 'angle: ' + current_car_angle + ', delta: ' + angle_delta + ', used: ' + throttle_to_use

        { msgType: 'throttle', data: throttle_to_use }

    decideThrottle: ( absolute_angle, angle_delta ) ->

        angle_increased = angle_delta > 0

        new_throttle = @last_throttle

        if angle_increased
            if angle_delta > 1 or absolute_angle > 15
                new_throttle *= 0.25
            else if (angle_delta >= 0.1)
                new_throttle *= 0.9
        else
            new_throttle += 0.25

        return new_throttle

    decideMaxThrottle: (pieces_to_next_curve) ->

        if pieces_to_next_curve >= 2
            1.00
        else if pieces_to_next_curve == 1
            0.82
        else
            0.765

    hello: =>
        'Grandma says: "Nyaggggh!"'