# Racecar bot logic

class CarPositions 
    constructor: (@track) ->

    update: (positions) ->
        

    getCarPosition: (id) ->



    getTrack: =>
        return @track     

    getMyPosition: (positions, id) ->
        for carPos in positions
            if carPos.id.color is id
                return carPos

    getCurrentPiece: (currentPos) ->
        @track.pieces[currentPos.piecePosition.pieceIndex]

    nextPieceIsBend: (currentPos) ->
        nextPiece = null

        if currentPos.piecePosition.pieceIndex + 1 < @track.pieces.length
            nextPiece = @track.pieces[currentPos.piecePosition.pieceIndex + 1]
        else
            nextPiece = @track.pieces[0] 

        return nextPiece.hasOwnProperty('angle')

exports.CarPositions = CarPositions

class Race
    construct: (@track, @cars) ->

    getCar: (id) ->



