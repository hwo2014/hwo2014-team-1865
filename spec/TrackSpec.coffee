Module = require('../coffeescript/track.coffee')
Track = Module.Track
Piece = Module.Piece

describe "A piece", ->

    it "should know whether it's a curve", ->
        piece = new Piece()

        expect(piece.isCurve()).toEqual(false)

        piece.setRadius(100)
        piece.setAngle(15)

        expect(piece.isCurve()).toEqual(true)

    it "should remember its values", ->
        length = 10.0
        has_switch = true
        radius = 200
        angle = 25.0

        piece = new Piece()

        piece.setLength(length)
        piece.setSwitch(has_switch)
        piece.setRadius(radius)
        piece.setAngle(angle)

        expect(piece.getLength()).toEqual(length)
        expect(piece.getSwitch()).toEqual(has_switch)
        expect(piece.getRadius()).toEqual(radius)
        expect(piece.getAngle()).toEqual(angle)

describe "A track", ->

    beforeEach ->
        @id = 'indianapolis'
        @track = new Track(@id)

    it "knows its details", ->

        name = 'Indianapolis'

        @track.setName(name)

        expect(@track.getId()).toEqual(@id)
        expect(@track.getName()).toEqual(name)

    describe "given two pieces", ->

        beforeEach ->

            @piece1 = new Piece()
            @piece2 = new Piece()

            @index_of_piece1 = @track.addPiece(@piece1)
            @index_of_piece2 = @track.addPiece(@piece2)

        it "gives logical index for both", ->

            expect(@index_of_piece1).toEqual(0)
            expect(@index_of_piece2).toEqual(1)

        it "stores correct index to each piece", ->

            expect(@piece1.getIndex()).toEqual(@index_of_piece1)
            expect(@piece2.getIndex()).toEqual(@index_of_piece2)

        it "stores them with consistent indexes", ->

            expect(@track.getPiece(@index_of_piece1)).toEqual(@piece1)
            expect(@track.getPiece(@index_of_piece2)).toEqual(@piece2)

        it "gives next piece of a track when asked for one", ->

            expect(@track.getPieceAfter(@index_of_piece1)).toEqual(@piece2)
            expect(@track.getPieceAfter(@index_of_piece2)).toEqual(@piece1)

        it "gives undefined is there is no curve in the track", ->

            index_of_piece_we_are_at = 0
            expect(@track.getPiecesToNextCurve(index_of_piece_we_are_at)).toBeUndefined()

        describe "given two pieces and a curve", ->

            beforeEach ->
                @curve_piece = new Piece()
                @curve_piece.setAngle(10.0)
                @curve_piece.setRadius(10.0)

                @track.addPiece(@curve_piece)

            it "gives correct distance to a curve if there is one", ->

                index_of_piece_we_are_at = 0
                known_number_of_pieces_in_between = 1

                expect(@track.getPiecesToNextCurve(index_of_piece_we_are_at)).toEqual(known_number_of_pieces_in_between)

            it "gives correct distance to a curve if we go around the track", ->

                index_of_piece_we_are_at = @curve_piece.getIndex()

                expect(@track.getPiecesToNextCurve(index_of_piece_we_are_at)).not.toBeUndefined()
