Module = require('../coffeescript/car.coffee')
Car = Module.Car

beforeEach ->
    @car_id = 'red'
    @car = new Car(@car_id)

describe "A car", ->

    it "should remember it's id", ->
        expect(@car.getId()).toEqual(@car_id)

    it "should not let throttle exceed the maximum set", ->
        max = 1.00
        throttle = max + 0.5

        @car.setMaxThrottle(max)
        @car.setThrottle(throttle)

        expect(@car.getMaxThrottle()).toEqual(@car.getThrottle())

    it "should not let throttle decrease below zero", ->
        @car.setThrottle(-0.5)

        expect(@car.getThrottle()).toEqual(0.00)

    it "should not have speed implementation", ->
        expect(@car.getSpeed()).toContain("not implemented")